import { Container } from "../mod.ts";
import type { Provider } from "../mod.ts";
import { Config } from "../core/Config.ts";
import { DelightApp, Middleware } from "../core/App.ts";

class AppFactory {
  constructor(
    private readonly rootPath: string,
    private readonly middlewares: Middleware[] = [],
  ) {}

  public with(middleware: Middleware): AppFactory {
    this.middlewares.push(middleware);
    return this;
  }

  public provide<T>(
    name: string,
    provider: Provider<T>,
    singleton = false,
  ): AppFactory {
    Container.Register<T>(name, provider, singleton);
    return this;
  }

  public async run(): Promise<void> {
    const config = await Config.fromDirectory(this.rootPath);
    const app = new DelightApp(this.rootPath, config, this.middlewares);
    return app.run();
  }
}

export function createApp(rootPath = Deno.cwd()): AppFactory {
  return new AppFactory(rootPath);
}
