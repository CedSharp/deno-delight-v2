export class AuthFactory {
  constructor(
    private headerName?: string,
    private useHeader = false,
    private useJwt = false,
  ) {}

  inHeader(headerName = "Bearer") {
    this.useHeader = true;
    this.headerName = headerName;
    return this;
  }

  withJWT() {
    this.useJwt = true;
    return this;
  }
}
