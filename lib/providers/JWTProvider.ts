import type { PrepareParams, Provider } from "../mod.ts";
import { Logger } from "../mod.ts";
import { JWT } from "../services/JWT.ts";

export class JWTProvider implements Provider<JWT> {
  private readonly logger = new Logger("JWTProvider");
  private secret = "notsecret";
  private algorithm = "HS256";
  private expiresIn = 3_600_000;

  public prepare({ config }: PrepareParams): void {
    this.secret = config.get("auth.secret", "secret");
    this.algorithm = config.get("auth.algorithm", "HS256");
    try {
      this.expiresIn = parseInt(config.get("auth.expiresIn", "3600000"));
    } catch (_) {
      this.logger.warn("Could not parse expiresIn, using default 3600000");
    }
  }

  public provide(): JWT {
    return new JWT(this.secret, this.algorithm, this.expiresIn);
  }
}
