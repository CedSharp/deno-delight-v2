export class ContainerNotPrepared extends Error {
  constructor() {
    super("Container was not prepared, yet tried to provide a value!");
    this.name = "ContainerNotPrepared";
  }
}
