// create InvalidJson class which extends Error
export class InvalidJson extends Error {
  constructor(jsonError: Error) {
    super(jsonError.message);
    this.name = "InvalidJson";
  }
}
