export class InvalidAuthConfig extends Error {
  constructor(reason?: string) {
    super(reason || "Unexpected error while configuring Auth Service");
    this.name = "InvalidAuthConfig";
  }
}
