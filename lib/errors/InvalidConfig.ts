export class InvalidConfig extends Error {
  constructor(key: string) {
    super(`"${key}" is not a valid config`);
    this.name = "InvalidConfig";
  }
}
