export class JWTEncodingError extends Error {
  constructor() {
    super("Something went wrong while encoding a token");
    this.name = "JWTEncodingError";
  }
}
