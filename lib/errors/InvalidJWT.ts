export class InvalidJWT extends Error {
  constructor() {
    super("Error while parsing a JWT token");
    this.name = "InvalidJWT";
  }
}
