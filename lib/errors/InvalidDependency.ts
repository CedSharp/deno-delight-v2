export class InvalidDependency extends Error {
  constructor(name: string) {
    super(`No dependency named ${name} are registered!`);
    this.name = "InvalidDependency";
  }
}
