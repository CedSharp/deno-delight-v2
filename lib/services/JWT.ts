import { decode, encode } from "https://deno.land/std/encoding/base64url.ts";
import { InvalidJWT, JWTEncodingError } from "../errors/mod.ts";
import { Logger } from "../mod.ts";

const bEnc = (data: Uint8Array): string => encode(data);
const bDec = (data: string): Uint8Array => decode(data);
const tEnc = (data: string): Uint8Array => new TextEncoder().encode(data);
const tDec = (data: Uint8Array): string => new TextDecoder().decode(data);

/*
 * NOTE: The secret is a base64url encoded key for HMAC SHA256,
 *       which needs tp be 256 characters.
 */

export class JWT {
  private readonly logger = new Logger("JWT Service");

  constructor(
    private readonly secret: string,
    private readonly algorithm = "HS256",
    private readonly expiresIn = 3_600_000,
  ) {}

  // Encode a payload into a JWT
  public async encode(data: unknown): Promise<string> {
    try {
      const header = { alg: this.algorithm, typ: "JWT" };
      const encodedHeader = bEnc(tEnc(JSON.stringify(header)));
      const encodedData = bEnc(tEnc(JSON.stringify(data)));
      const payload = `${encodedHeader}.${encodedData}`;
      const signature = await this.sign(payload);
      return `${payload}.${bEnc(tEnc(signature))}`;
    } catch (err) {
      this.logger.error(err.name, err.message);
      throw new JWTEncodingError();
    }
  }

  private getKey(): Promise<CryptoKey> {
    return crypto.subtle.importKey(
      "raw",
      decode(this.secret),
      { name: "HMAC", hash: { name: "SHA-256" } },
      false,
      ["sign", "verify"],
    );
  }

  private async sign(data: string): Promise<string> {
    return bEnc(
      new Uint8Array(
        await crypto.subtle.sign(
          {
            name: "HMAC",
            hash: { name: "SHA-256" },
          },
          await this.getKey(),
          tEnc(data),
        ),
      ),
    );
  }

  public async verify(token: string): Promise<unknown | null> {
    const [payload, signature] = token.split(".");
    const decodedSignature = bDec(signature);
    const verified = await crypto.subtle.verify(
      {
        name: "HMAC",
        hash: { name: "SHA-256" },
      },
      await this.getKey(),
      decodedSignature,
      tEnc(payload),
    );
    if (!verified) {
      this.logger.error("Invalid JWT signature");
      throw new InvalidJWT();
    }
    try {
      return JSON.parse(tDec(bDec(payload)));
    } catch (err) {
      this.logger.error(err.name, err.message);
      throw new InvalidJWT();
    }
  }
}
