import { Container, Request } from "../mod.ts";
import { InvalidAuthConfig } from "../errors/mod.ts";
import { JWT } from "./JWT.ts";

export interface AuthAdapter {
  authenticate(request: Request): Promise<boolean>;
  storePayload(request: Request, payload: unknown): Promise<void>;
  retrievePayload(request: Request): Promise<unknown | null>;
}

export class Auth {
  constructor(
    private readonly headerName: string,
    private readonly useHeader: boolean,
    private readonly useJwt: boolean,
  ) {
    if (this.useHeader && this.headerName === "") {
      throw new InvalidAuthConfig(
        "Auth header name must be set when using auth with headers",
      );
    }
  }

  public async getPayloadForRequest<T = unknown>(
    request: Request,
  ): Promise<T | null> {
    const token = this.getEncodedToken(request);
    const payload = await this.verify(token);
    return payload === null ? null : payload as T;
  }

  private getEncodedToken(request: Request): string {
    let encodedToken = "";
    if (this.useHeader) {
      encodedToken = request.getHeader(this.headerName) || "";
    }
    return encodedToken;
  }

  private getJwt(): JWT {
    return Container.Get<JWT>("JWT");
  }

  private verify(token: string): Promise<unknown | null> {
    if (this.useJwt) return this.getJwt().verify(token);
    return Promise.resolve(null);
  }
}
