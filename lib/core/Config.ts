import { join } from "https://deno.land/std/path/mod.ts";
import { parse } from "https://deno.land/std/encoding/yaml.ts";
import { InvalidConfig } from "../errors/mod.ts";

function isYamlFile(file: string): boolean {
  return file.endsWith(".yaml") || file.endsWith(".yml");
}

function flatten(
  entries: [string, Record<string, unknown>][],
): Record<string, unknown> {
  return entries.reduce((acc, [file, yaml]) => {
    acc[file] = yaml;
    return acc;
  }, {} as Record<string, unknown>);
}

/** Parses the yaml files in a directory and provides an api to consume them */
export class Config {
  constructor(private config: Record<string, unknown>) {}

  /**
   * Finds and parse all yaml files in a directory and returns
   * a flattened Config
   */
  public static async fromDirectory(directory: string): Promise<Config> {
    // Check if a config directory exists, otherwise look for config in root
    if (Deno.statSync(join(directory, "config")).isDirectory) {
      directory = join(directory, "config");
    }

    // Get all the yaml files in the directory
    const readConfigs: Promise<[string, Record<string, unknown>]>[] = [];
    for await (const file of Deno.readDir(directory)) {
      if (file.isFile && isYamlFile(file.name)) {
        readConfigs.push(
          // Read the file and parse it as yaml
          Deno.readTextFile(`${directory}/${file.name}`)
            .then((text) => parse(text) as Record<string, unknown>)
            .then((yaml) => [file.name.replace(/\.ya?ml$/, ""), yaml]),
        );
      }
    }

    // Flatten all the parsed yaml files into a single config
    const configs = await Promise.all(readConfigs);
    return new Config(flatten(configs));
  }

  /** Retrieve a value from the configuration */
  public get<T>(dots: string, defaultValue: T | undefined = undefined): T {
    const keys = dots.split(".");
    try {
      return keys.reduce((config: unknown, key: string) => {
        if (typeof config !== "object") throw new InvalidConfig(dots);
        if (!config || !(key in config)) throw new InvalidConfig(dots);
        return (config as Record<string, unknown>)[key];
      }, this.config) as T;
    } catch (error) {
      if (error instanceof InvalidConfig && defaultValue !== undefined) {
        return defaultValue as T;
      }
      throw error;
    }
  }
}
