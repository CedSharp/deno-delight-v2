import { Config } from "../core/Config.ts";
import { Container, Logger, Request } from "../mod.ts";
import { white } from "https://deno.land/std/fmt/colors.ts";

export type MiddlewareContext = {
  readonly app: typeof Container;
  readonly request: Request;
  readonly next: () => Promise<void>;
};

export type Middleware = (context: MiddlewareContext) => Promise<void>;

export class DelightApp {
  constructor(
    private readonly rootPath: string,
    private readonly config: Config,
    private readonly middlewares: Middleware[],
    private readonly logger = new Logger("Delight App"),
  ) {}

  public async run(): Promise<void> {
    Container.Prepare();
    this.logger.heading("Delight   ━━━   0.2", true);

    const host = this.config.get<string>("app.host", "localhost");
    const port = this.config.get<number>("app.port", 8080);

    const listener = Deno.listen({ hostname: host, port: port });
    this.logger.info(`Server listening on ${white(`${host}:${port}`)}`);

    for await (const conn of listener) {
      const httpConn = Deno.serveHttp(conn);
      for await (const requestEvent of httpConn) {
        await this.handleRequest(requestEvent);
      }
    }
  }

  private async executeMiddlewares(request: Request): Promise<void> {
    let currentMiddleware = 0;
    const getNextMiddleware = (): Middleware | null => {
      const nextMiddleware = currentMiddleware++;
      if (nextMiddleware < this.middlewares.length) {
        const middleware = this.middlewares[nextMiddleware];
        return middleware;
      }
      return null;
    };

    const context: MiddlewareContext = {
      app: Container,
      request,
      next: async () => {
        const nextMiddleware = getNextMiddleware();
        if (nextMiddleware) await nextMiddleware.apply(null, [context]);
      },
    };

    await context.next();
  }

  private async handleRequest(requestEvent: Deno.RequestEvent): Promise<void> {
    const request = new Request(requestEvent);
    await request.waitForBody();
    await this.executeMiddlewares(request);
  }
}
