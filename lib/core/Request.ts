import { InvalidJson } from "../errors/mod.ts";

export class Request {
  private _href!: string;
  private _domain!: string;
  private _port = 80;
  private _path!: string;
  private _searchParams!: URLSearchParams;
  private _readBody!: Promise<void>;
  private _rawBody: string | null = null;
  private _headers: Headers;

  constructor(reqEvent: Deno.RequestEvent) {
    this.parseUrl(reqEvent.request.url);
    this._headers = reqEvent.request.headers;
    this.parseBody(reqEvent.request.body);
  }

  public get href(): string {
    return this._href;
  }

  public get domain(): string {
    return this._domain;
  }

  public get port(): number {
    return this._port;
  }

  public get path(): string {
    return this._path;
  }

  public get searchParams(): URLSearchParams {
    return this._searchParams;
  }

  public getHeader(name: string): string | null {
    return this._headers.get(name);
  }

  public async waitForBody(): Promise<void> {
    await this._readBody;
  }

  public getBody(): string {
    if (this._rawBody !== null) return this._rawBody;
    throw new Error("Body has not been read yet");
  }

  public getBodyAsJson(): unknown {
    const body = this.getBody();
    try {
      return JSON.parse(body);
    } catch (err) {
      throw new InvalidJson(err);
    }
  }

  private parseUrl(url: string): void {
    const { hostname, pathname, port, searchParams } = new URL(url);
    this._href = url;
    this._path = pathname;
    this._searchParams = searchParams;
    this._domain = hostname;
    if (port) this._port = parseInt(port);
  }

  private parseBody(body: ReadableStream<Uint8Array> | null): void {
    this._readBody = new Promise((resolve, reject) => {
      if (body === null) {
        this._rawBody = "";
        return resolve();
      }

      const reader = body.getReader();
      const decoder = new TextDecoder();

      const read = async () => {
        let done = false;
        let rawBody = "";

        while (!done) {
          const result = await reader.read();
          if (result.done) done = true;
          else rawBody += decoder.decode(result.value);
        }

        console.log({ done, rawBody });
        this._rawBody = rawBody;
      };

      read().then(resolve, reject);
    });
  }
}
