import { sprintf } from "https://deno.land/std/fmt/printf.ts";
import {
  cyan,
  gray,
  green,
  red,
  yellow,
} from "https://deno.land/std/fmt/colors.ts";

const boxChars = {
  topLeft: "┌",
  topRight: "┐",
  bottomLeft: "└",
  bottomRight: "┘",
  horizontal: "─",
  vertical: "│",
};

const boldBoxChars = {
  topLeft: "╔",
  topRight: "╗",
  bottomLeft: "╚",
  bottomRight: "╝",
  horizontal: "═",
  vertical: "║",
};

export class Logger {
  constructor(private readonly prefix: string) {}

  public heading(title: string, bold = false, margin = 2): void {
    const { columns } = Deno.consoleSize(Deno.stdout.rid);
    const chars = bold ? boldBoxChars : boxChars;
    const innerWidth = columns - margin * 2 - 2;
    const m = " ".repeat(margin);
    const left = Math.max(0, Math.floor((innerWidth - title.length) / 2));
    const right = Math.max(0, innerWidth - title.length - left);
    const horizontal = chars.horizontal.repeat(innerWidth);
    const topLine = `${m}${chars.topLeft}${horizontal}${chars.topRight}`;
    const centerLine = `${m}${chars.vertical}${" ".repeat(left)}${title}${
      " ".repeat(right)
    }${chars.vertical}`;
    const bottomLine =
      `${m}${chars.bottomLeft}${horizontal}${chars.bottomRight}`;
    console.log(`\n${topLine}\n${centerLine}\n${bottomLine}\n`);
  }

  public debug(message: string, ...args: unknown[]): void {
    this.log(gray(message), ...args);
  }

  public info(message: string, ...args: unknown[]): void {
    this.log(cyan(message), ...args);
  }

  public warn(message: string, ...args: unknown[]): void {
    this.log(yellow(message), ...args);
  }

  public error(message: string, ...args: unknown[]): void {
    this.log(red(message), ...args);
  }

  public success(message: string, ...args: unknown[]): void {
    this.log(green(message), ...args);
  }

  private log(message: string, ...args: unknown[]): void {
    console.log(sprintf(`${gray(`[${this.prefix}]`)} ${message}`, ...args));
  }
}
