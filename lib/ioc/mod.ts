import { ContainerNotPrepared, InvalidDependency } from "../errors/mod.ts";
import { Config } from "../core/Config.ts";

/** The parameters provider to `Provider.prepare()` */
export type PrepareParams = {
  readonly namespace: string;
  readonly app: Container;
  readonly config: Config;
};

/** Contract used by the Container to register providers */
export interface Provider<T> {
  prepare?: (params: PrepareParams) => void;
  provide: () => T;
}

/** Represents a binded name to a {@link Provider} */
type Binding<T> = {
  provider: Provider<T>;
  isSingleton: boolean;
  cachedValue?: T | null;
};

/** IoC Container allowing to bind strings to {@link Provider}s */
export class Container {
  private static prepared = false;
  private static entries = new Map<string, Binding<unknown>>();

  static Register<T>(
    name: string,
    provider: Provider<T>,
    singleton = false,
  ): void {
    this.entries.set(name, {
      provider,
      isSingleton: singleton,
    });
  }

  static Get<T = unknown>(name: string): T {
    if (!this.prepared) throw new ContainerNotPrepared();
    if (!this.entries.has(name)) throw new InvalidDependency(name);
    return this.Resolve(this.entries.get(name)!) as T;
  }

  static Has(name: string): boolean {
    return this.entries.has(name);
  }

  static Prepare(config: Config): void {
    if (this.prepared) return;

    const params = {
      namespace: "",
      app: this,
      config,
    };

    for (const [name, entry] of this.entries.entries()) {
      if (entry.provider.prepare) {
        params.namespace = name;
        entry.provider.prepare.call(entry.provider, params);
      }
    }

    this.prepared = true;
  }

  static Clear(): void {
    this.entries.clear();
    this.prepared = false;
  }

  private static Resolve(binding: Binding<unknown>): unknown {
    if (binding.isSingleton && binding.cachedValue) {
      return binding.cachedValue;
    }
    binding.cachedValue = binding.provider.provide.call(binding.provider);
    return binding.cachedValue;
  }
}
