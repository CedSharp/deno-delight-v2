import { assert, assertThrows } from "https://deno.land/std/testing/asserts.ts";
import { Container } from "../lib/mod.ts";
import type { Provider } from "../lib/mod.ts";
import { ContainerNotPrepared, InvalidDependency } from "../lib/errors/mod.ts";

class Greeter {
  constructor() {}
}

class GreeterProvider implements Provider<Greeter> {
  provide() {
    return new Greeter();
  }
}

Deno.test("Inversion of Control", async ({ step }) => {
  await step("allows registering providers", () => {
    Container.Clear();
    Container.Register("Greeter", new GreeterProvider());
    assert(Container.Has("Greeter"));
  });

  await step("resolves different instances when not singleton", () => {
    Container.Clear();
    Container.Register("Greeter", new GreeterProvider());
    Container.Prepare();
    const greeter1 = Container.Get("Greeter") as Greeter;
    const greeter2 = Container.Get("Greeter") as Greeter;
    assert(greeter1 !== greeter2);
  });

  await step("resolves the same instance when singleton", () => {
    Container.Clear();
    Container.Register("Greeter", new GreeterProvider(), true);
    Container.Prepare();
    const greeter1 = Container.Get("Greeter") as Greeter;
    const greeter2 = Container.Get("Greeter") as Greeter;
    assert(greeter1 === greeter2);
  });

  await step("throws an error if trying to resolve without preparing", () => {
    Container.Clear();
    Container.Register("Greeter", new GreeterProvider(), true);
    assertThrows(() => Container.Get("Greeter"), ContainerNotPrepared);
  });

  await step("throws an error if trying resolve an invalid provider", () => {
    Container.Clear();
    Container.Prepare();
    assertThrows(() => Container.Get("A"), InvalidDependency);
  });
});
