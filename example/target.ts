// deno-lint-ignore-file

import { dirname, fromFileUrl } from "https://deno.land/std/path/mod.ts";

import {
  createApp,
  createAuth,
  createRouter,
} from "https://deno.cedsharp.ca/delight/mod.ts";

import UserController from "./controllers/UserController.ts";

const auth = createAuth()
  .inHeader("Authentification")
  .withJWT();

const router = createRouter()
  .get("/", (ctx) => ctx.text("Welcome!"))
  .get("/show/:name", (ctx) => ctx.text(`Showing ${ctx.params.name}`))
  .post("/", (ctx) => ctx.json({ message: "Invalid..." }))
  .controller("/api/users", UserController);

createApp(dirname(fromFileUrl(import.meta.url)))
  .with(auth)
  .with(router)
  .run();
