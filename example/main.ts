import { dirname, fromFileUrl } from "https://deno.land/std/path/mod.ts";
import { createApp, Logger } from "../lib/mod.ts";
import { Greeter, useGreeter } from "./providers/mod.ts";
import type { Middleware } from "../lib/mod.ts";

const __dirname = dirname(fromFileUrl(import.meta.url));
const logger = new Logger("Example");

const handleHomepage: Middleware = async ({ request, next }) => {
  if (request.path === "/") logger.info("Homepage");
  else await next();
};

const handleGreeter: Middleware = async ({ app, next, request }) => {
  if (request.path === "/greet") {
    const greeter = app.Get<Greeter>("greeter");
    logger.info(greeter.greet());
  } else await next();
};

// deno-lint-ignore require-await
const handle404: Middleware = async ({ request }) => {
  logger.info(`404: ${request.path}`);
};

await createApp(__dirname)
  .provide("greeter", useGreeter("Ced"))
  .with(handleHomepage)
  .with(handleGreeter)
  .with(handle404)
  .run();
