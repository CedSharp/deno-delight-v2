import type { Provider } from "./mod.ts";

// create class greeter which greets provided name
export class Greeter {
  constructor(private name: string) {}

  greet() {
    return `Hello ${this.name}`;
  }
}

// Create a provider class which provides greeter
class GreeterProvider implements Provider<Greeter> {
  constructor(private name: string) {}

  provide() {
    return new Greeter(this.name);
  }
}

export function useGreeter(name: string): GreeterProvider {
  return new GreeterProvider(name);
}
