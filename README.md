# Deno Delight

A web micro framework built using Deno. The goal was simply to learn and have
fun using Deno.

It's not optimized, debugged and does not have any documentation yet, but it
"works".

This repository is the [2nd time](https://gitlab.com/cedsharp/deno-delight.git)
I try to do this, I wanted to change some of the structures I had in the first
iteration but it would require a good rewrite.

## Goal

I want to build something that allows me to prototype an idea very quickly
without having to bother too much about implementation or security.

It's also a good opportunity to learn, and I want to include the following
components in this framework:

- [x] Basic http request/response handling
- [x] IoC Containers
- [x] Configurable
- [x] Middleware
- [ ] Authentication
- [ ] Router with dynamic parameter support
- [ ] Controllers for the Router
- [ ] ORM to interact with a database
- [ ] Models for the ORM

**NOTE**: All of these features are at their very simplest form. I'm not trying
to remake something like Laravel or AdonisJS.

## Installation

The project is not ready to be used on https://deno.land yet, so the only way to
use it is to clone this repository and import what's needed from `lib/mod.ts`.

1. First, clone the repository on your machine:

```console
$ git clone https://gitlab.com/CedSharp/deno-delight-v2.git delight
```

2. Next, create a folder where your project should reside:

```console
$ mkdir my-project
$ cd my-project
```

3. Import what you need from `lib/mod.ts`, refer to `example/` for usage:

```typescript
// my-project/main.ts

import { dirname, fromFileUrl } from "https://deno.land/std/path/mod.ts";
import { createApp, Logger } from "../delight/lib/mod.ts";
import type { Middleware } from "../delight/lib/mod.ts";

const __dirname = dirname(fromFileUrl(import.meta.url));
const logger = new Logger("My Project");

const handleHomepage: Middleware = async ({ request, next }) => {
  if (request.path === "/") logger.info("Homepage");
  else await next();
};

// deno-lint-ignore require-await
const handle404: Middleware = async ({ request }) => {
  logger.info(`404: ${request.path}`);
};

createApp(__dirname)
  .with(handleHomepage)
  .with(handle404)
  .run();
```

4. Finally, you can run your project using the following flags:

```console
$ deno run --unstable --allow-net --allow-read main.ts
```
